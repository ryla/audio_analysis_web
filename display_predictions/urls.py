from django.urls import path
from display_predictions import views

urlpatterns = [
    path('', views.hello_world, name='display_predictions'),
]