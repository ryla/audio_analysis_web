from django.shortcuts import render
from django.core.files.storage import FileSystemStorage

import numpy as np
import os
import requests
from io import BytesIO
import base64
import json

import matplotlib.pyplot as plt
import librosa
import librosa.display

import matplotlib
matplotlib.use("Agg")


# The API for prediction
API_ENDPOINT = 'http://127.0.0.1:5000/'

labels_dict_3labels = {1: 'Neutral/Positive', 2: 'Inaudible', 3: 'Negative'}
labels_dict_4labels = {1: 'Neutral/Positive', 2: 'Inaudible', 3: 'Sad', 4: 'Angry'}
labels_dict_5labels = {0: 'Neutral', 1: 'Positive', 2: 'Inaudible', 3: 'Sad', 4: 'Angry'}

proba = False


def hello_world(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()

        filename = fs.save(myfile.name, myfile)
        audio_data, sample_rate = librosa.load(os.path.join('static/audio', filename))

        wave_plot = plot_wave(audio_data, sample_rate)
        spec_plot = plot_spectogram(audio_data, sample_rate)

        n_labels = int(request.POST.get('n_labels'))
        data = {'audio_data': audio_data.tolist(),
                'sample_rate': sample_rate,
                'n_labels': n_labels,
                'type_features': request.POST.get('type_features'),
                'proba': proba}

        # sending post request and saving response as response object
        # TODO : Should handle status 500 (server error)
        response = requests.post(url=API_ENDPOINT + 'predict', json=data)
        response = json.loads(response.text)

        predictions = format_predictions(response, get_labels_dict(n_labels), proba)

        if proba:
            radar_plot = plot_radar(predictions['labels'], predictions['probas'])

            return render(request, 'display_predictions.html', {
                'filename': filename,
                'radar_plot': radar_plot,
                'wave_plot': wave_plot,
                'spec_plot': spec_plot,
                'n_labels': n_labels
            })
        else:
            return render(request, 'display_predictions.html', {
                'filename': filename,
                'predictions': predictions,
                'wave_plot': wave_plot,
                'spec_plot': spec_plot,
                'n_labels': n_labels
            })

    return render(request, 'display_predictions.html')


def format_predictions(response, labels_dict, proba=False):
    predictions = {}

    if proba:
        labels = []
        for _, label in labels_dict.items():
            labels.append(label)
        predictions['labels'] = labels

        probas_per_model = {}
        for model, prediction in response.items():
            probas = []
            for label, proba_pred in prediction.items():
                probas.append(proba_pred)
            probas_per_model[model] = probas
        predictions['probas'] = probas_per_model

    else:
        for model, pred in response.items():
            predictions[model] = labels_dict[int(pred)]

    return predictions


def get_labels_dict(n_labels):
    if n_labels == 3:
        labels_dict = labels_dict_3labels
    elif n_labels == 4:
        labels_dict = labels_dict_4labels
    else:
        labels_dict = labels_dict_5labels

    return labels_dict


def plot_wave(audio_data, sample_rate):
    # Construct the graph
    plt.figure()
    librosa.display.waveplot(audio_data, sr=sample_rate)
    plt.title('Amplitude envelope of the recordings\'s waveform')

    # Store image in a bytes buffer
    buffer = BytesIO()

    plt.savefig(buffer, format='png', dpi=300)
    wave_plot = base64.b64encode(buffer.getvalue()).decode('utf-8').replace('\n', '')

    return wave_plot


def plot_spectogram(audio_data, sample_rate):
    d = librosa.amplitude_to_db(np.abs(librosa.stft(audio_data)), ref=np.max)
    plt.figure()
    librosa.display.specshow(d, sr=sample_rate, y_axis='log')
    plt.title('Spectogram')

    buffer = BytesIO()
    plt.savefig(buffer, format='png', dpi=300)
    spec_plot = base64.b64encode(buffer.getvalue()).decode('utf-8').replace('\n', '')

    return spec_plot


def plot_radar(labels, probas):
    angles = np.linspace(0, 2 * np.pi, len(labels), endpoint=False)
    angles = np.concatenate((angles, [angles[0]]))

    fig = plt.figure(figsize=(20, 5))

    for i, (model, pred) in enumerate(probas.items()):
        # close the plot
        pred = np.concatenate((pred, [pred[0]]))

        ax = plt.subplot(100+10*len(probas)+(i+1), projection='polar')

        ax.plot(angles, pred, 'o-', linewidth=2)
        ax.fill(angles, pred, alpha=0.25)
        ax.set_thetagrids(angles * 180 / np.pi, labels)
        ax.set_title(model)
        ax.grid(True)

    buffer = BytesIO()
    fig.savefig(buffer, format='png', dpi=300)
    radar_plot = base64.b64encode(buffer.getvalue()).decode('utf-8').replace('\n', '')

    return radar_plot
