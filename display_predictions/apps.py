from django.apps import AppConfig


class HelloWorldConfig(AppConfig):
    name = 'display_predictions'
